import observerPattern.Boss;
import observerPattern.Worker;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/6
 **/
public class Test {

    public static void main (String[] agrs) {
        Boss boss = new Boss();

        Worker worker = new Worker();
        worker.registerObserver(boss);
        worker.doWork();
    }
}

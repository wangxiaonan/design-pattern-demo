package observerPattern;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/6
 **/
public class Boss implements Observer {
    public void update(Object... args) {
        for (Object o : args) {
            System.out.println(o.toString());
            System.out.println("好，你去财务结算一下工资，明天别来了！");
        }
    }
}

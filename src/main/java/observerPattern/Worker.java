package observerPattern;

/**
 * @Author: wangxiaonan
 * @Date: 2018/7/6
 **/
public class Worker extends Subject{
    public void doWork(){
        System.out.println("---------------------------");

        notifyAllObserver("老板我要涨工资");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("我还有活，我先回去了...");
    }
}

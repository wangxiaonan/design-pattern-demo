
package observerPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 被观察者
 *
 * @Author: wangxiaonan
 * @Date: 2018/7/6
 **/
public abstract class Subject {
    private static List<Observer> list = new ArrayList<Observer>();

    public void registerObserver(Observer o) {
        list.add(o);
    }

    public void removeObserver(Observer o) {
        list.remove(o);
    }

    public void notifyAllObserver(Object... objects) {
        for (Observer o : list) {
            o.update(objects);
        }
    }
}

package observerPattern;

/**
 * 观察者
 *
 * @Author: wangxiaonan
 * @Date: 2018/7/6
 **/
public interface Observer {
    void update(Object... args);
}
